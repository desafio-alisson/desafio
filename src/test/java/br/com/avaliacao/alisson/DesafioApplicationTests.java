package br.com.avaliacao.alisson;

import br.com.avaliacao.alisson.controller.RotaController;
import br.com.avaliacao.alisson.controller.dto.CadastroRotaDTO;
import br.com.avaliacao.alisson.repository.model.Aeroporto;
import br.com.avaliacao.alisson.repository.model.Rota;
import br.com.avaliacao.alisson.repository.model.base.BaseDeDados;
import br.com.avaliacao.alisson.repository.RotaRepository;
import br.com.avaliacao.alisson.repository.model.base.MenorPreco;
import br.com.avaliacao.alisson.service.RotaService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Optional;

import static org.mockito.Mockito.when;

@SpringBootTest(args = "teste")
class DesafioApplicationTests {

	@Mock
	private RotaService rotaService;

	@Autowired
	private RotaController controller;

	@Autowired
	private RotaRepository repository;

	@Test
	public void rotaServiceEncotraMelhorRotaTest(){
		Rota mockRota = new Rota();
		when(rotaService.encontrarMelhorRota("GRU","CDG")).thenReturn(mockRota);
	}

	@Test
	public void novaRotaJaExiseTest(){
		CadastroRotaDTO cadastroRotaDTO = new CadastroRotaDTO();
		cadastroRotaDTO.setOrigem("SCL");
		cadastroRotaDTO.setDestino("BRC");
		cadastroRotaDTO.setValor(new BigDecimal("86"));
		Assertions.assertTrue(repository.exist(cadastroRotaDTO));
	}

	@Test
	public void rotaControllerCadastroErroTest()  {

		CadastroRotaDTO cadastroRotaDTO = new CadastroRotaDTO();
		cadastroRotaDTO.setOrigem("SCL");
		cadastroRotaDTO.setDestino("BRC");
		cadastroRotaDTO.setValor(new BigDecimal("86"));

		try {
			controller.cadastrar(cadastroRotaDTO);
			Assertions.assertTrue(false);
		}catch (Exception e){
			Assertions.assertTrue(true);
		}

	}

	@Test
	public void rotaControllerMelhorRotaTest()  {

		try {
			Rota rota = controller.consultaMelhorRota("GRU", "CDG");
			Assertions.assertTrue(new BigDecimal("40").equals(rota.getTotal()));
			Assertions.assertTrue(rota.getAeroportos().size() == 5);
		}catch (Exception e){
			e.printStackTrace();
			Assertions.assertTrue(false);
		}

	}

	@Test
	public void rotaControllerMelhorRotaErroRotaNaoEncontradaTest()  {

		try {
			Rota rota = controller.consultaMelhorRota("GRU", "AAA");
			Assertions.assertTrue(false);
		}catch (Exception e){
			Assertions.assertTrue(true);
		}

	}


	@Test
	public  void melhorRotaTest(){
		MenorPreco menorPreco = new MenorPreco(BaseDeDados.getBase());
		Aeroporto origem = new Aeroporto("GRU");
		Aeroporto destino = new Aeroporto("CDG");
		Optional<Rota> oRota = menorPreco.encotrarMelhorRota(origem, destino);
		Assertions.assertTrue(oRota.isPresent());
		if(oRota.isPresent()){
			Assertions.assertTrue(oRota.get().getTotal().equals(new BigDecimal("40")));
			Assertions.assertTrue(oRota.get().getAeroportos().size() == 5);
		}
	}

}
