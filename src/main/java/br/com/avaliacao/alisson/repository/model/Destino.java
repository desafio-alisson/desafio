package br.com.avaliacao.alisson.repository.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Destino {
    private Aeroporto aeroporto;
    private BigDecimal valor;

    public Destino(Aeroporto aeroporto, BigDecimal valor){
        this.aeroporto = aeroporto;
        this.valor = valor;
    }

    public BigDecimal getValor(){
        return valor;
    }

    public Aeroporto getAeroporto() {
        return aeroporto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Destino destino = (Destino) o;
        return Objects.equals(aeroporto, destino.aeroporto) &&
                Objects.equals(valor, destino.valor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(aeroporto, valor);
    }
}
