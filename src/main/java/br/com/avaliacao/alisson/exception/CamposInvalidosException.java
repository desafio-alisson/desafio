package br.com.avaliacao.alisson.exception;

import br.com.avaliacao.alisson.controller.dto.CampoInvalido;

import java.util.List;

public class CamposInvalidosException extends RuntimeException {
    private List<CampoInvalido> campos;

    public CamposInvalidosException(List<CampoInvalido> campos){
        this.campos = campos;
    }

    public List<CampoInvalido> getCampos() {
        return campos;
    }
}
