package br.com.avaliacao.alisson.exception;

public class InternoErroException extends RuntimeException {

    public InternoErroException(String mensagem){
        super(mensagem);
    }
}
