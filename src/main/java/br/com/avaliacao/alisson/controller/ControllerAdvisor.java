package br.com.avaliacao.alisson.controller;

import br.com.avaliacao.alisson.controller.dto.CampoInvalido;
import br.com.avaliacao.alisson.exception.CamposInvalidosException;
import br.com.avaliacao.alisson.controller.dto.MensagemDTO;
import br.com.avaliacao.alisson.exception.InternoErroException;
import br.com.avaliacao.alisson.exception.RotaNaoEncontradaException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

@RestControllerAdvice
public class ControllerAdvisor {

    @ExceptionHandler(CamposInvalidosException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<CampoInvalido> handleCamposInvalidos(CamposInvalidosException ex){
        return ex.getCampos();
    }

    @ExceptionHandler(InternoErroException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public MensagemDTO handleInernoErro(InternoErroException ex){
        return new MensagemDTO(ex.getMessage());
    }

    @ExceptionHandler(RotaNaoEncontradaException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public MensagemDTO handleInernoErro(RotaNaoEncontradaException ex){
        return new MensagemDTO(ex.getMessage());
    }

}
