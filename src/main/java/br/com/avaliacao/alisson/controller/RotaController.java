package br.com.avaliacao.alisson.controller;

import br.com.avaliacao.alisson.controller.dto.CadastroRotaDTO;
import br.com.avaliacao.alisson.controller.dto.CampoInvalido;
import br.com.avaliacao.alisson.controller.dto.MensagemDTO;
import br.com.avaliacao.alisson.exception.CamposInvalidosException;
import br.com.avaliacao.alisson.repository.model.Rota;
import br.com.avaliacao.alisson.service.RotaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("rotas")
@Api("Api Rota de Viagem")
public class RotaController {

    @Autowired
    private RotaService rotaService;

    @PostMapping
    @ApiOperation(value = "Efetua o cadastro de novas rotas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Caso a rota seja cadastrada com sucesso"),
            @ApiResponse(code = 400, message = "Caso Algum dado de entrada esteja nulo ou a rota já estiver cadastrada"),
    })
    public MensagemDTO cadastrar(@RequestBody CadastroRotaDTO rotaDTO){
        validaRota(rotaDTO);
        rotaService.cadastrar(rotaDTO);
        return new MensagemDTO("Rota cadastrada com sucesso");
    }

    @GetMapping("melhor-rota")
    @ApiOperation(value = "Encontra melhor preço entre a rota origem e destino")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna a lista de pessoa"),
            @ApiResponse(code = 400, message = "Caso Algum dado de entrada esteja nulo ou Vazio"),
            @ApiResponse(code = 404, message = "Caso não encontre nenhuma rota entre origem e destino"),
    })
    public Rota consultaMelhorRota(@RequestParam("origem") String origem, @RequestParam("destino") String destino){
        validaEntrada(origem, destino);
        return rotaService.encontrarMelhorRota(origem.trim().toUpperCase(), destino.trim().toUpperCase());
    }

    private void validaEntrada(String origem, String destino) {
        List<CampoInvalido> camposInvalidos = new ArrayList<>();

        if(Objects.isNull(origem) || origem.isEmpty()){
            camposInvalidos.add(new CampoInvalido("origem", "Campo de prenenchimento obrigatório"));
        }

        if(Objects.isNull(destino) || destino.isEmpty()){
            camposInvalidos.add(new CampoInvalido("destino", "Campo de prenenchimento obrigatório"));
        }

        if(!camposInvalidos.isEmpty()){
            throw new CamposInvalidosException(camposInvalidos);
        }
    }

    private void validaRota(CadastroRotaDTO rotaDTO) {
        List<CampoInvalido> camposInvalidos = new ArrayList<>();

        if(Objects.isNull(rotaDTO.getOrigem()) || rotaDTO.getOrigem().isEmpty()){
            camposInvalidos.add(new CampoInvalido("origem", "Campo de prenenchimento obrigatório"));
        }

        if(Objects.isNull(rotaDTO.getDestino()) || rotaDTO.getOrigem().isEmpty()){
            camposInvalidos.add(new CampoInvalido("destino", "Campo de prenenchimento obrigatório"));
        }

        if(Objects.isNull(rotaDTO.getValor())){
            camposInvalidos.add(new CampoInvalido("valor", "Campo de prenenchimento obrigatório"));
        }

        if(!camposInvalidos.isEmpty()){
            throw new CamposInvalidosException(camposInvalidos);
        }
    }

}
