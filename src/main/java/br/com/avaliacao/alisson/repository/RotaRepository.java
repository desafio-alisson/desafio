package br.com.avaliacao.alisson.repository;

import br.com.avaliacao.alisson.controller.dto.CadastroRotaDTO;
import br.com.avaliacao.alisson.exception.InternoErroException;
import br.com.avaliacao.alisson.repository.model.Aeroporto;
import br.com.avaliacao.alisson.repository.model.Destino;
import br.com.avaliacao.alisson.repository.model.Rota;
import br.com.avaliacao.alisson.repository.model.base.BaseDeDados;
import br.com.avaliacao.alisson.repository.model.base.MenorPreco;
import org.springframework.stereotype.Repository;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.*;

@Repository
public class RotaRepository {

    public synchronized void cadastrar(CadastroRotaDTO rotaDTO) {

        try(BufferedWriter writer = Files.newBufferedWriter(BaseDeDados.getPathFile(), StandardOpenOption.APPEND)){
            writer.newLine();
            String linha = tranformeLinha(rotaDTO);
            writer.write(linha);
            atualizaMap(rotaDTO);
        } catch (IOException e) {
            e.printStackTrace();
            throw new InternoErroException("Falha ao tentar persistir a nova rota");
        }
    }

    public Optional<Rota> encontraMelhorRota(Aeroporto aeroportoOrigem, Aeroporto aeroportoDestino) {
        MenorPreco menorPreco = new MenorPreco(BaseDeDados.getBase());
        return menorPreco.encotrarMelhorRota(aeroportoOrigem, aeroportoDestino);
    }

    public boolean exist(CadastroRotaDTO rotaDTO){
        Aeroporto origem = new Aeroporto(rotaDTO.getOrigem().trim());
        List<Destino> destinos = BaseDeDados.getBase().get(origem);
        if(Objects.nonNull(destinos)){
            Destino destino = new Destino(new Aeroporto(rotaDTO.getDestino().trim()), rotaDTO.getValor());
            return destinos.contains(destino);
        }
        return false;
    }

    private String tranformeLinha(CadastroRotaDTO rotaDTO) {
        StringBuffer sb = new StringBuffer();
        sb.append(rotaDTO.getOrigem().trim()+","+ rotaDTO.getDestino().trim()+","+ rotaDTO.getValor().toString());
        return sb.toString();
    }

    private void atualizaMap(CadastroRotaDTO rotaDTO) {
        Aeroporto origem = new Aeroporto(rotaDTO.getOrigem().trim());
        Destino destino = new Destino(new Aeroporto(rotaDTO.getDestino().trim()), rotaDTO.getValor());
        Map<Aeroporto, List<Destino>> map = BaseDeDados.getBase();
        if(Objects.isNull(map.get(origem))){
            List<Destino> listaDestinos = new ArrayList<>();
            listaDestinos.add(destino);
            map.put(origem, listaDestinos);
        }else{
            map.get(origem).add(destino);
        }
    }

}
