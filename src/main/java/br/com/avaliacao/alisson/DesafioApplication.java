package br.com.avaliacao.alisson;

import br.com.avaliacao.alisson.repository.model.base.BaseDeDados;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

@SpringBootApplication
public class DesafioApplication implements ApplicationRunner {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(DesafioApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {

		String pathDir = null;
		if(args.getSourceArgs().length == 1 && "teste".equals(args.getSourceArgs()[0])){
			pathDir = getClass().getResource("../../../../rota_aviao.csv").getPath();
		}else if(args.getSourceArgs().length != 1){
			throw new RuntimeException("Arquivo csv não foi informado");
		}else{
			pathDir =  args.getSourceArgs()[0];
		}

		File file = new File(pathDir);
		validaArquivo(file);

		System.out.println("Lendo dados do Arquivo");
		BaseDeDados.load(file.toPath());
		System.out.println("Leitura Finalizada");
	}

	private  void validaArquivo(File file) throws IOException {
		if(!file.getName().endsWith("csv")){
			throw new IOException("Somente é aceito arquivo com extenção csv");
		}

		if(!file.exists()){
			throw new FileNotFoundException("Arquivo não encontrado no seguinte caminho="+file.getAbsolutePath());
		}
	}
}
