package br.com.avaliacao.alisson.exception;

public class RotaNaoEncontradaException extends RuntimeException {
    public RotaNaoEncontradaException(){
        super("Rota não encontrada");
    }
}
