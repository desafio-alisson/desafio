package br.com.avaliacao.alisson.service;

import br.com.avaliacao.alisson.controller.dto.CadastroRotaDTO;
import br.com.avaliacao.alisson.controller.dto.CampoInvalido;
import br.com.avaliacao.alisson.exception.CamposInvalidosException;
import br.com.avaliacao.alisson.exception.RotaNaoEncontradaException;
import br.com.avaliacao.alisson.repository.model.Aeroporto;
import br.com.avaliacao.alisson.repository.model.Rota;
import br.com.avaliacao.alisson.repository.RotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Service
public class RotaService {

    @Autowired
    private RotaRepository repository;

    public void cadastrar(CadastroRotaDTO rotaDTO) {
        if(repository.exist(rotaDTO)){
            throw new CamposInvalidosException(Arrays.asList(new CampoInvalido("", "Rota já cadastrada.")));
        }
        repository.cadastrar(rotaDTO);
    }

    public Rota encontrarMelhorRota(String origem, String destino){
        RotaRepository repository = new RotaRepository();
        Aeroporto aeroportoOrigem = new Aeroporto(origem);
        Aeroporto aeroportoDestino = new Aeroporto(destino);
        Optional<Rota> oRota = repository.encontraMelhorRota(aeroportoOrigem, aeroportoDestino);

        if(oRota.isPresent()){
            return oRota.get();
        }
        throw new RotaNaoEncontradaException();
    }
}
